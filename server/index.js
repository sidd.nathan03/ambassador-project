var path = require('path'); 
var express = require('express');
var logger = require('morgan');
var cors = require('cors');
var app = express();
const multer = require('multer');
var bodyParser = require('body-parser')


// nicely log http requests to terminal
app.use(logger('dev'));

// serve folder with userContent statically
app.use(express.static('uploads'));


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});
const upload = multer({storage: storage});

/*var fileFilter = (req, file, cb) => {
    console.log("File Filter");
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}*/


// api subrouter
var router = express.Router();
// default data
//const memes  =  [{image:"http://localhost:8000/api/uploads/default.png", text: "a funny joke"}, {image:"http://localhost:8000/api/uploads/default.png", text: "the punchline"}, {image:"http://localhost:8000/api/uploads/default.png", text: "the applause"}];
// json body parser 
router.use(express.json())
// allow cors so frontend dev can access api
router.use(cors())

// retrieve a list of very fine memes
router.get('/memes', function(req, res) {
    res.json(memes)
});

// logs request and returns request body
router.post('/log', function(req, res) {
    console.log((req.body))
    res.json(req.body)
});

// add the router to the api subroute
app.use('/api', router)
app.use(bodyParser.json());
app.use('/uploads',express.static('uploads'));
// serve static frontend build
app.use( express.static('build'));

// if other paths dont match serve index.html for single page app routing 
app.use(function(req, res) {
    res.sendFile(path.join(__dirname,'../build/index.html'));
});

const memes  =  [{image:"http://localhost:8000/image1.png", text: "image from Server"}];
router.post('/upload', upload.single('image'), (req, res, next) => {

    try {
        return res.status(201).json(memes);
    } catch (error) {
        console.error(error);
    }
});




app.listen(8000, function() {
    console.log("Listening on port 8000");

})

