import {createStore, combineReducers} from 'redux';

export const TOGGLE_TITLE_COLOR = 'TOGGLE_TITLE_COLOR';


//ACTION
export const toggleTitleColor = () => ({
    type: TOGGLE_TITLE_COLOR,
});
export const overlayLabel = (otext) => ({
    type: 'ENTER_OVERLAY',
    payload: otext

});



const defaultState = {
    titleColor: 'primary'
}


const defaultOverlay = 'HELLO WORLD';
function memeState(state=defaultState, action ){
  //REDUCER
    switch(action.type){
      
        case TOGGLE_TITLE_COLOR:
            return {
                ...state,
                titleColor: state.titleColor === 'primary' ? 'secondary' : 'primary',
            };

        default:
            return state;
    }
}

function overlayFunc(state=defaultOverlay, action ){
    //REDUCER
      switch(action.type){
        
          case 'ENTER_OVERLAY':
              return action.payload
  
          default:
              return state;
      }
  }
//include All Reducers
const root = combineReducers({memeState, overlayFunc})

export const store = createStore(root,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

 