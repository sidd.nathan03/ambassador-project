import React, {  useEffect, useState } from "react";
import injectSheet  from "react-jss";
import { Route, Link, Switch, Redirect } from "react-router-dom";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { useSelector, useDispatch } from 'react-redux'
import {toggleTitleColor} from './store';


import {overlayLabel} from './store';


import Axios from "axios";




const styles = theme => ({
  '@global body': {
    background: theme.palette.background,
    color: theme.palette.text
  },
  App: {
    padding: '20px',
    background: 'black',
    maxWidth: '800px',
    minHeight: '600px',
    margin: 'auto',
    '&  h1': {
      fontSize: '5rem',
      textAlign: 'center',
      fontFamily: 'serif',
      cursor: 'pointer'
    },
    '& input': {
      margin: '10px'
    },
    '& a': {
      color: theme.palette.text,
    },
    '& p': {
      fontSize: '1.2rem',
      color: 'white',
      fontFamily: 'Helvetica, Arial, sans-serif',
      lineHeight: '1.2'
    },
    '& button': {
      backgroundColor: '#F1DF99', 
      border: 'none',
      color: 'black',
      padding: '15px 32px',
      textAlign: 'center',
      textDecoration: 'none',
      display: 'inline-block',
      fontSize: '16px',
      fontFamily: 'Segoe UI',
      borderRadius: '5px',
      display:'inline-block',
      margin: '10px',
      
    },
    '& input[type=text]': {
      width: '50%',
      padding: '10px 5px',
      margin: '10px 10px',
      display: 'inline-block',
      border: '1px solid #ccc',
      boxSizing: 'border-box',
      fontFamily: 'Segoe UI'
    },
    '& figcaption': {
      fontFamily: 'serif',
      textAlign: 'left',
      fontSize: '1em', 
      '&:before, &:after':{
        display: 'inline',
        content: "\""
      }
    }
  },
  'title-primary': {
    color: theme.palette.primary
  },
  'title-secondary': {
    color: theme.palette.secondary
  },

  
});

const stp = (state) => ({
  titleColor: state.memeState.titleColor
})

const dtp = (dispatch) => bindActionCreators( {
  toggleTitleColor: () => toggleTitleColor()
}, dispatch)



export const App = (props) => {
  
    const { classes, titleColor, toggleTitleColor } = props;

    const [memes, setMemes] = useState([]);
    const [readme, setReadMe] = useState([]);
    
    const overlaySelector = useSelector(state => state.overlayFunc)

    const dispatch = useDispatch();

   const myCanvas = document.querySelector("#c");
   const upVideo = document.querySelector("#v");
   const imgConverted = document.querySelector("#imgConverted");

    useEffect( () => {
      fetch('http://localhost:8000/api/memes/').then( res => res.json()).then( setMemes);
    }, [])
    
 
    console.info("memes", memes)

    

    //Create and Display  MEME
    function handleClick(e) {
      e.preventDefault();
      var formText = e.target.overlay.value;
      console.log('The link was clicked.'+ formText);
      dispatch(overlayLabel(formText))
      console.log("State Data is " + {overlaySelector});
      var canvas = document.getElementById("c");
      var video = document.getElementById("v");
      console.log("Video Paused"); 
      
      if (video.paused) {
          var context = canvas.getContext("2d");
          context.drawImage(video, 0, 0);   
          context.font = "40pt Calibri";
          context.fillStyle = "white";
          context.fillText(formText, 50, 50);
         
          

      }
      
    }

    //Create and Display  MEME for uploaded video
    function handleClickUpload(e) {
      e.preventDefault();
      var formText = e.target.overlay.value;
      console.log('The link was clicked.'+ formText);
      dispatch(overlayLabel(formText))
      console.log("State Data is " + {overlaySelector});
      var canvas = document.getElementById("c");
      var video = document.getElementById("vid");
      console.log("Video Paused"); 
      
      if (video.paused) {
          var context = canvas.getContext("2d");
          context.drawImage(video, 0, 0);   
          context.font = "40pt Calibri";
          context.fillStyle = "white";
          context.fillText(formText, 50, 50);
         
          

      }
      
    }
    
    
    //DOwnloading the MEME Image
    function btnDownload(e) {
      
      const a = document.createElement("a");
      document.body.appendChild(a);
      a.href = myCanvas.toDataURL("image/jpeg", 0.92);
      a.download = "canvas-img.jpg";
      a.click();
      document.body.removeChild(a);
    }

   


   //executes when file is uploaded by the user
   function onFileChange(e) {
      console.log(e.target.files[0]);
      var video = document.getElementById('vid');
      var source = document.getElementById('source');

      source.setAttribute('src', e.target.files[0]);

      video.load();
      video.play();

   }
// Uploading the Image to Server
   function upload(e) {
    
    const dataURI = myCanvas.toDataURL("image/jpeg", 0.92);
    const fd =new FormData();
    fd.append('image', dataURItoBlob(dataURI), "image1.png");
    
    Axios.post('http://localhost:8000/api/upload/', fd)
    .then(res => {
      window.alert("File Uploaded to server Successfully")
      console.log(res);


    });

    

   }


   function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);
  
    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
  
    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
  
    // create a view into the buffer
    var ia = new Uint8Array(ab);
  
    // set the bytes of the buffer to the correct values
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
  
    // write the ArrayBuffer to a blob, and you're done
    var blob = new Blob([ab], {type: mimeString});
    return blob;
  
  }
   

  
    
    return (
      
      <div className={classes.App}>
        <header className="App-header">
          <h1 onClick={toggleTitleColor} className={ classes[`title-${titleColor}`] }>Vintage Meme Machine</h1>
        </header>
        <main>
          <Switch>
            <Route path="/" exact>
              <>
              
              <video id="v" autoPlay="autoPlay" crossOrigin="crossorigin"  controls>
    <source  src="https://upload.wikimedia.org/wikipedia/en/transcoded/6/61/Old_Man_Drinking_a_Glass_of_Beer_%281897%29.webm/Old_Man_Drinking_a_Glass_of_Beer_%281897%29.webm.360p.webm" type="video/ogg"/>
</video>

             
                <form id="form1" onSubmit= {handleClick}>
                  <p>Instructions: </p><p> 1. Pause the video at the point where you want to insert the text.</p><p>2. Enter the text in the Text box and Click Create Meme</p>
                <p>
                  Meme text:
                  <input type="text" name = "overlay" placeholder="Something edgy..." />
                 
                  
                  <button type="submit"  value=" Create MEME"  className="button" >Create MEME</button><br/> </p>
                </form>
                
                <button type=  "button" id="btnDownload" onClick={btnDownload}>Download as Image File</button>
                <button type=  "button" id="btnUpload" onClick={upload}>Upload</button>

               
    <p>State Data: {overlaySelector}</p>
    <p>3. Click Download button to download the Meme as Image File</p>
    <p>4. Click Upload to upload the image to the server</p>
    <p>5. To retrieve the image from server goto <a href = "http://localhost:8000/image1.png">http://localhost:8000/image1.png</a></p>
    <p>Meme Image:</p>
    <canvas crossOrigin="Anonymous" id="c" width="500" height="500"></canvas>
    
        <img src="" id="imgConverted" ></img>
        <p> User Video Upload</p>
        <video id="vid"  autoPlay="autoPlay" crossOrigin="crossorigin"  controls>
          <source src = "" id = "source" type = "video/webm"/>
        </video>
        <input type="file" onChange={onFileChange} /> 
        <form id="form2" onSubmit= {handleClickUpload}>
                <p>
                  Meme text:
                  <input type="text" name = "overlay" placeholder="Something edgy..." />
                 
                  
                  <button type="submit"  value=" Create MEME"  className="button" >Create MEME</button><br/> </p>
                </form>

          <button type=  "button" id="btnDownload" onClick={btnDownload}>Download as Image File</button>
                <button type=  "button" id="btnUpload" onClick={upload}>Upload</button>
                
               
                
              </>
            </Route>
            <Route path="/memes">
              <>
                <ul>
                  {memes.map( (meme) => {
                    return (
                    <li>
                      <figure>
                      <a href={meme.image} download="memeimage"></a>
                      <img src={meme.image} alt="meme image" />
                      <figcaption>{meme.text}</figcaption>
                      
                      </figure>
                      
                    </li>
                    
                    
                    
                    
                    )
                  })}
                                      <p>State Data: {overlaySelector}</p>

                </ul>
                
              </>
            </Route>
            <Route path="/readme">
              <section>
                <h2>Devtest Readme</h2>
                <p>
                  Hello candidate, Welcome to our little dev test. The goal of
                  this exercise, is to asses your general skill level, and give
                  us something to talk about at our next appointment.
                </p>
                <section>
                  <h3>What this app should do</h3>
                  <p>
                    We'd like for you to build a tiny app called the "Vintage Meme
                    Machine". It will be a tool that allows users to overlay text
                    on a video, and capture or share it.
                  </p>
                  <p>These are the basic requirements:</p>
                  <ul>
                    <li>User can pick a point in the video</li>
                    <li>
                      User can enter text that is placed over the video <em>still</em>
                    </li>
                    <li>
                      User can save this personalized content to the server, for later use 
                    </li>
                    <li>
                      User can retrieve her meme via an url 
                    </li>
                  </ul>
                </section>
                <section>
                  <h3>What we want you to do</h3>
                  <p>
                    Off course we don't expect you to build a full fledged app
                    in such a short time frame.
                  </p>
                  <p>
                    But we would like for you to get in the basic requirements,
                    in one form or another. Beyond that feel free to show off
                    your strenghts as a developer.
                  </p>
                  <p>Some ideas:</p>
                  <ul>
                    <li>Make it look really nice</li>
                    <li>Allow users to provide a custom video</li>
                    <li>Make the server secure enough to deploy</li>
                    <li>Download the meme as an image file</li>
                    <li>Add super cool text effects to the text overlay</li>
                    <li>Push the resulting meme to a social media API</li>                    
                  </ul>
                </section>
                <section>
                  <p>
                    P.s. We've already added some libraries to make your life
                    easier (Create React App, Redux, Jss, React Router, Express), but feel free to add
                    more.
                  </p>
                </section>
              </section>
            </Route>
            <Redirect to="/" />
          </Switch>
        </main>
        <footer>
          <nav>
            <ul>
            <li><Link to="/home">Home</Link></li>
            <li><Link to="/memes">Memes</Link></li>
            <li><Link to="/readme">Readme</Link></li>
            </ul>
          </nav>
        </footer>
      </div>
    );
};


export default connect(stp,dtp)(injectSheet(styles)(App));
